import { ref, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

//component
import userService from '@/services/user'
import { useLoadingStore } from '@/stores/loading'
import type { UserSend } from '@/types/UserSend'
export const useUserStore = defineStore('user', () => {
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loadingStore = useLoadingStore()

  const initiUser: User = {
    fullName: '',
    email: '',
    password: '',
    gender: 'male'
  }

  const editedUser = ref<User>(JSON.parse(JSON.stringify(initiUser)))

  const users = ref<User[]>([])

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    loadingStore.finish()
  }

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      clearForm()
    })
  }

  async function deleteItemConfirm() {
    await deleteUser()
    closeDelete()
  }

  async function deleteUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const res = await userService.delUser(user)
    await getUsers()
    clearForm()
    loadingStore.finish()
  }

  async function editItem(item: User) {
    if (!item.id) return
    await getUser(item.id)
    dialog.value = true
  }

  async function deleteItem(item: User) {
    if (!item.id) return
    await getUser(item.id)
    dialogDelete.value = true
  }

  async function saveUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    if (!user.id) {
      //Add new
      // const sendUser: UserSend = {
      //   email: user.email,
      //   password: user.password,
      //   fullName: user.fullName,
      //   gender: user.gender
      // }
      // console.log(sendUser)
      const res = await userService.addUser(user)
    } else {
      //Update
      const res = await userService.updateUser(user)
    }
    getUsers()
    clearForm()
    loadingStore.finish()
  }

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initiUser))
  }

  return {
    users,
    dialog,
    dialogDelete,
    initiUser,
    editedUser,
    deleteItemConfirm,
    editItem,
    deleteItem,
    closeDelete,
    getUsers,
    saveUser,
    deleteUser,
    clearForm
  }
})
