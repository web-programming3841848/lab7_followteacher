type UserSend = {
  email: string
  password: string
  fullName: string
  gender: 'male' | 'female' | 'others' // Male, Female, Others
  //roles: Role[] // admin, user
}

export type { UserSend }
